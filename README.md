**master at:** raspi4
# winston-wrap

[![](https://badgen.net/packagephobia/install/winston-wrap)](https://packagephobia.now.sh/result?p=winston-wrap) [![](https://badgen.net/packagephobia/publish/winston-wrap)](https://packagephobia.now.sh/result?p=winston-wrap)

This library is a simple wrapper around `winston` logger initialisation.
Default config returns a logger with 2 transports (file and console) pre-configured.

## Setup

	npm install winston-wrap

## Usage

`winston-wrap` exposes a single function; simply pass to this function a label name, and, why not, some options (as shown bellow) and it will return a `winston` logger object ready to be used.

### Default Usage

With no parameters passed to the require, 2 transports are created with the following pre-configured caracteristics :

*console* :
```js
config.file = {
	level: 'debug',
	filename: `./output-${appName}.log`,
	handleExceptions: true,
	maxsize: 5242880, // 5MB
	maxFiles: 5,
	format: format,
};
```

*file* :
```js
config.console = {
	handleExceptions: true,
	format: combine(
		logformat.colorize(),
		format,
	),
};
```

*colors* :
```js
config.colors = {
	error: 'red',
	warn: 'magenta',
	info: 'green',
	verbose: 'yellow',
	debug: 'cyan',
	silly: 'white',
};
```

*finally, the logger* :
```js
config.logger = {
	level: 'debug',
	exitOnError: false, // do not exit on handled exceptions
	transports: [
		new transports.File(config.file),
		new transports.Console(config.console),
	],
};
```

In short, logs are sent to a file (filename: `./output-${appName}.log`,) and simultaneously to the console (with some colorization for the latter).

*Example with all defaults* :
```js
const logger = require('winston-wrap');
const options = { file: null, console: { level: 'verbose' } }

logger.debug('options %o', options);
```
will output the following text in console and in `./output.log` :

```
2019-01-02 14:40:30 [debug]: options { file: null, console: { level: 'verbose' } }
```

### Usage with a label

If you pass a string to the require creating the logger, you will add a label to each log line. This label can be used to identify the module which generated the line :

Logs are still sent to a file (filename: `./output-${appName}.log`,) and to the console.

*Example* :
```js
const logger = require('winston-wrap')('index.js');
const options = { file: null, console: { level: 'verbose' } }

logger.debug('options %o', options);
```
will output the following text in console and in ./output.log :
```
2019-01-02 14:40:30 [debug]: index.js - options { file: null, console: { level: 'verbose' } }
```

### Usage with winston options

You can also pass an option object when calling the require. This object accepts the following attributes :
```js
options = {
	file: {},
	console: {},
	colors: {},
	logger:{},
}
```

These attributes will simply be merged with the default config (described above) to allow you to overide it if needed.
If you want to deactivate a transport, just affect a `null` attribute.

The configuration of each sub-object has to follow `winston` rules as shown in their [documentation](https://github.com/winstonjs/winston).
`file` is a `transport.File` configuration object, `console` a `transport.Console` one, etc...

*Example 1* :
```js
const options = { file: null, console: { level: 'verbose' } }
const logger = require('winston-wrap')('index.js', options);

logger.debug('options %o', options);
```
will output nothing, ...anywhere.
(file output is disabled and console output is elevated to verbose)

*Example 2* :

```js
const options = { file: null, console: { level: 'verbose' } }
const logger = require('winston-wrap')('index.js', options);

logger.warn('options %o', options);
```
will output the following to the console (and no file) :

```
2019-01-02 14:40:30 [warn]: index.js - options { file: null, console: { level: 'verbose' } }
```

*Example 3* :

```js
const app = 'myApp';
const options = { file: { filename: `/home/pi/output-${app}.log` } }
const logger = require('winston-wrap')('index.js', options);

logger.info('options %o', options);
```
will output anything to the console and the following to an app dedicated output log in your `/home/pi` directory :

```
2019-01-02 14:40:30 [info]: index.js - options { file: { filename: '/home/pi/output-myApp.log' } }
```
