const createLogger = require('winston').createLogger;
const logformat = require('winston').format;
const transports = require('winston').transports;
const addColors = require('winston').addColors;
const moment = require('moment-timezone');
const fs = require('fs');

function setup(namespace, options) {
	/**
	* Create a debugger with the given `namespace`.
	*
	* @param {String} namespace
	* @param {Object} options
	* @return {Function}
	* @api public
	*/
	function createDebug(namespace, options) {
		function debug() {}

		debug.namespace = namespace;

		const {	combine, label, printf, splat } = logformat;

		const timestamp = logformat((info, opts) => {
			const info1 = info;
			if(opts.tz){
				info1.timestamp = moment().tz(opts.tz).format('YYYY-MM-DD HH:mm:ss');
			}
			return info1;
		});

		let format;
		if(namespace) {
			format = combine(
				splat(),
				timestamp({ tz: moment.tz.guess() }),
				label({ label: namespace }),
				printf(info => `${info.timestamp} [${info.level}]: ${info.label} - ${info.message}`),
			);
		} else {
			format = combine(
				splat(),
				timestamp({ tz: moment.tz.guess() }),
				printf(info => `${info.timestamp} [${info.level}]: ${info.message}`),
			);
		}

		const packPath = `${__dirname}/../../package.json`;
		let outputPath = './output.log';
		if(fs.existsSync(packPath)) {
			outputPath = `./output-${require(packPath).name}.log`;
		}

		const config = {};
		config.file = {
			level: 'debug',
			filename: outputPath,
			handleExceptions: true,
			maxsize: 5242880, // 5MB
			maxFiles: 5,
			format: format,
		};

		if(options) {
			if(options.file) {
				Object.assign(config.file, options.file);
			} else if(options.file===null) {
				delete config.file;
			}
		}

		config.console = {
			handleExceptions: true,
			format: combine(
				logformat.colorize(),
				format,
			),
		};

		if(options) {
			if(options.console) {
				Object.assign(config.console, options.console);
			} else if(options.console===null) {
				delete config.console;
			}
		}

		config.transports = [];
		if(config.file) {
			config.transports.push(new transports.File(config.file));
		}
		if(config.console) {
			config.transports.push(new transports.Console(config.console));
		}

		config.logger = {
			level: 'debug',
			exitOnError: false, // do not exit on handled exceptions
			transports: config.transports,
		};

		if(options) {
			if(options.logger) {
				Object.assign(config.logger, options.logger);
			}
		}

		config.colors = {
			error: 'red',
			warn: 'magenta',
			info: 'green',
			verbose: 'yellow',
			debug: 'cyan',
			silly: 'white',
		};

		if(options) {
			if(options.colors) {
				Object.assign(config.colors, options.colors);
			} else if(options.colors===null) {
				delete config.colors;
			}
		}

		if(config.colors) {
			addColors(config.colors);
		}

		return createLogger(config.logger);
	}

	return createDebug(namespace, options);
}

module.exports = setup;
